<?php
namespace vendor\pillax\simpleException\src;

interface ExceptionInterface {

    public function __construct($code, array $details);

    public function getDetails();
}