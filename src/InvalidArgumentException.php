<?php
namespace vendor\pillax\simpleException\src;

class InvalidArgumentException extends \Exception implements ExceptionInterface {
    private $details = [];

    public function __construct($message, array $details = []) {
        $this->details = $details;
        parent::__construct($message);
    }

    public function getDetails() {
        return $this->details;
    }
}