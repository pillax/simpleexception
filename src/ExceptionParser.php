<?php
namespace vendor\pillax\simpleException\src;

use vendor\pillax\Arr;

class ExceptionParser {

    /** @var array Error data */
    private static $data = [];

    /**
     * Parse exception in compact format;
     * Makes environment snapshot;
     * Set data to self::$data
     * Returns instance of current class
     *
     * @param \Throwable $e
     * @return ExceptionParser
     */
    public static function init(\Throwable $e) : self {
        self::$data['message'] = $e->getMessage();
        self::$data['code'] = $e->getCode();
        self::$data['where'] = $e->getFile() . ':' . $e->getLine();

        $trace = $e->getTrace();
        foreach ($trace AS $element) {
            self::$data['trace'][] = [
                'where' => Arr::get($element, 'file', '?') . ':' . Arr::get($element, 'line', '?'),
                'func' => Arr::get($element, 'class', '?') . ' ' . Arr::get($element, 'function', '?'),
                'args' => Arr::get($element, 'args'),
            ];
        }

        self::$data['env']['_POST']     = $_POST;
        self::$data['env']['_GET']      = $_GET;
        self::$data['env']['_COOKIE']   = $_COOKIE;
        self::$data['env']['_SESSION']  = isset($_SESSION) ? $_SESSION : null;

        self::$data['memory'] = round((memory_get_usage() / 1048576), 2) . 'M from ' . ini_get('memory_limit');

        return new self();
    }

    /**
     * Display collected exception data
     */
    public function show() : void {
        var_export(self::$data);
    }

    /**
     * Save collected exception data to log file
     */
    public function log() : void {
        dd('TODO');
    }

    /**
     * Send collected exception data to email
     */
    public function mail() : void {
        dd('TODO');
    }
}